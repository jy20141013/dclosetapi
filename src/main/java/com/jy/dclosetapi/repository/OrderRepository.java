package com.jy.dclosetapi.repository;

import com.jy.dclosetapi.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Orders, Long> {
}
