package com.jy.dclosetapi.repository;
import com.jy.dclosetapi.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsRepository extends JpaRepository<Goods,Long> {

}
