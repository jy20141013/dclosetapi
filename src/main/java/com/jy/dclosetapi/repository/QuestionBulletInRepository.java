package com.jy.dclosetapi.repository;

import com.jy.dclosetapi.entity.QuestionBulletIn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionBulletInRepository extends JpaRepository <QuestionBulletIn,Long> {
}
