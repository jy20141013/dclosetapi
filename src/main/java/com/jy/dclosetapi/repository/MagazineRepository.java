package com.jy.dclosetapi.repository;

import com.jy.dclosetapi.entity.Magazine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MagazineRepository extends JpaRepository<Magazine, Long> {
}
