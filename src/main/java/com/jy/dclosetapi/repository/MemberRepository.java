package com.jy.dclosetapi.repository;

import com.jy.dclosetapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByUsername(String username);
}
