package com.jy.dclosetapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DclosetApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DclosetApiApplication.class, args);
    }

}
