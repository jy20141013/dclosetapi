package com.jy.dclosetapi.entity;

import com.jy.dclosetapi.enums.GoodsSize;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
public class Goods {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime productCreateDate;

    @Column(nullable = false,length = 20)
    private String productName;

    @Column(nullable = false)
    private Integer productCode;

    @Column(nullable = false,columnDefinition = "text")
    private String productInfo;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private GoodsSize goodsSize;

    @Column(nullable = false, length = 10)
    private String productColor;

    @Column(nullable = true)
    private String productMainImage;
    @Column(nullable = true)
    private String productSubImage1;
    @Column(nullable = true)
    private String productSubImage2;
    @Column(nullable = true)
    private String productSubImage3;

    @Column(nullable = false)
    private Boolean YnMembership;

    @Column(nullable = false)
    private Boolean YnOffline;

    @Column(nullable = false)
    private Boolean YnFree;

    @Column(nullable = false)
    private Boolean YnOneDay;

    @Column(nullable = false)
    private Boolean YnPost;

    @Column(nullable = false)
    private Integer useAbleQuantity;

    @Column(nullable = false)
    private Integer usingQuantity;

    @Column(nullable = false)
    private Integer repairQuantity;



}
