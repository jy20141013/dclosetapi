package com.jy.dclosetapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class QuestionBulletIn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false)
    private LocalDate questionCreateDate;

    @Column(nullable = false)
    private String questionTitle;

    @Column(nullable = false)
    private Integer questionPassword;

    @Column(columnDefinition = "Text",nullable = false)
    private String questionContent;


}
