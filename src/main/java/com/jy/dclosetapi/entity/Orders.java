package com.jy.dclosetapi.entity;

import com.jy.dclosetapi.enums.MemberGrade;
import com.jy.dclosetapi.enums.OrderStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId")
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goodsId")
    private Goods goods;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MemberGrade memberGrade;

    @Column(nullable = false)
    private LocalDate orderDate;

    @Column(nullable = false)
    private LocalDate desiredDate;

    @Column(nullable = false)
    private LocalDate deadlineDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private OrderStatus orderStatus;

    @Column(nullable = true, length = 40)
    private String deliveryId;

    @Column(nullable = true, length = 40)
    private String returnId;

}
