package com.jy.dclosetapi.entity;

import com.jy.dclosetapi.enums.MemberGrade;
import com.jy.dclosetapi.enums.MemberGroup;
import com.jy.dclosetapi.enums.PayDay;
import com.jy.dclosetapi.enums.PayWay;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String passwordRe;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(nullable = false, length = 50)
    private String memberAddress;

    @Column(nullable = false, length = 50)
    private String memberDetailedAddress;

    @Column(nullable = false)
    private Integer postCode;

    @Column(nullable = false)
    private LocalDateTime subscriptDate;

    @Column(nullable = false)
    private Boolean NYPersonalInfo;

    @Column(nullable = false)
    private Boolean NYMarketing;

    @Column(nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private MemberGrade memberGrade;

    @Column(nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private PayWay payWay;

    @Column(nullable = true, length = 100)
    private String payInfo;

    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private PayDay payDay;
}
