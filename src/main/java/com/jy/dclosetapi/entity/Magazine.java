package com.jy.dclosetapi.entity;

import com.jy.dclosetapi.enums.MemberGroup;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Magazine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false)
    private LocalDate magazineCreateDate;

    @Column(nullable = false, length = 50)
    private String magazineTitle;

    @Column(nullable = false, length = 30)
    private String magazineImage;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String magazineText;

    @Column(nullable = false, length = 30)
    private String curatorRecommend1;

    @Column(nullable = false, length = 30)
    private String curatorRecommend2;

    @Column(nullable = false, length = 30)
    private String curatorRecommend3;

    @Column(nullable = false, length = 30)
    private String curatorRecommend4;
}
