package com.jy.dclosetapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "디클로젯 APP",
                description = "당신만의 드레스룸, 디클로젯",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi loveLineApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("디클로젯 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
