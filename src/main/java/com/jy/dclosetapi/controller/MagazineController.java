package com.jy.dclosetapi.controller;

import com.jy.dclosetapi.entity.Member;
import com.jy.dclosetapi.model.magazine.MagazineChangeRequest;
import com.jy.dclosetapi.model.magazine.MagazineCreateRequest;
import com.jy.dclosetapi.model.magazine.MagazineItem;
import com.jy.dclosetapi.model.magazine.MagazineResponse;
import com.jy.dclosetapi.service.MagazineService;
import com.jy.dclosetapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/magazine")
public class MagazineController {
    private final MagazineService magazineService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    public String setMagazine(@PathVariable long memberId, MagazineCreateRequest request) {
        Member member = memberService.getMember(memberId);
        magazineService.setMagazine(member, request);
        return "OK";
    }

    @GetMapping("/all")
    public List<MagazineItem> getMagazines() {
        return magazineService.getMagazines();
    }

    @GetMapping("/detail/{id}")
    public MagazineResponse getMagazine(@PathVariable long id) {
        return magazineService.getMagazine(id);
    }

    @PutMapping("/change/{id}")
    public String putMagazine(@PathVariable long id, @RequestBody MagazineChangeRequest request) {
        magazineService.putMagazine(id, request);
        return "OK";
    }
}
