package com.jy.dclosetapi.controller;

import com.jy.dclosetapi.model.goods.GoodsCreateRequest;
import com.jy.dclosetapi.service.GoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
public class GoodsController {
    private final GoodsService goodsService;

    public String setGoods(GoodsCreateRequest request) {
        goodsService.setGoods(request);
        return "OK";
    }
}
