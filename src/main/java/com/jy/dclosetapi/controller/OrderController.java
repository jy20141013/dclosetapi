package com.jy.dclosetapi.controller;

import com.jy.dclosetapi.entity.Member;
import com.jy.dclosetapi.entity.Goods;
import com.jy.dclosetapi.model.order.OrderItem;
import com.jy.dclosetapi.model.order.OrderRequest;
import com.jy.dclosetapi.service.GoodsService;
import com.jy.dclosetapi.service.MemberService;
import com.jy.dclosetapi.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/order")
public class OrderController {
    private final OrderService orderService;
    private final MemberService memberService;
    private final GoodsService goodsService;

    @PostMapping("/new/member-id/{memberId}/goods-id/{goodsId}")
    public String setOrder(@PathVariable long memberId, long goodsId, @RequestBody OrderRequest orderRequest) {
        Member member = memberService.getMember(memberId);
        Goods goods = goodsService.getGoods(goodsId);
        orderService.setOrder(member, goods, orderRequest);

        return "OK";
    }

    @GetMapping("/all")
    public List<OrderItem> getOrders() {
        return orderService.getOrders();
    }
}
