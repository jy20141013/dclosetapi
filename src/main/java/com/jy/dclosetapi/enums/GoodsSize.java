package com.jy.dclosetapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GoodsSize {

    SMALL("S"),
    MEDIUM("M"),
    LARGE("L");

    private final String goodsSize;


}
