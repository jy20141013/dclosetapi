package com.jy.dclosetapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGrade {
    NORMAL("일반 회원"),
    MEMBERSHIP("멤버십 회원"),
    NONE("비회원");

    private final String memberGrade;
}
