package com.jy.dclosetapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatus {
    RECEIVE("주문 접수"),
    CHECKING("주문 확인 중"),
    PREPARING("상품 준비 중"),
    SHIPMENT("상품 출고"),
    USING("대여 중"),
    RETURN_RECEIVE("반납 신청"),
    COMPLETE("대여 완료");

    private final String orderStatus;
}
