package com.jy.dclosetapi.service;

import com.jy.dclosetapi.entity.Goods;
import com.jy.dclosetapi.model.goods.GoodsCreateRequest;
import com.jy.dclosetapi.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class GoodsService {
    private final GoodsRepository goodsRepository;

    public Goods getGoods(long id) {
        return goodsRepository.findById(id).orElseThrow();
    }

    public void setGoods(GoodsCreateRequest request) {
        Goods addData = new Goods();
        addData.setProductCreateDate(LocalDateTime.now());
        addData.setProductCode(request.getProductCode());
        addData.setProductInfo(request.getProductInfo());
        addData.setGoodsSize(request.getGoodsSize());
        addData.setProductColor(request.getProductColor());
        addData.setProductMainImage(request.getProductMainImage());
        addData.setProductSubImage1(request.getProductSubImage1());
        addData.setProductSubImage2(request.getProductSubImage2());
        addData.setProductSubImage3(request.getProductSubImage3());
        addData.setYnMembership(request.getYnMembership());
        addData.setYnOffline(request.getYnFree());
        addData.setYnOneDay(request.getYnOneDay());
        addData.setYnPost(request.getYnPost());
        addData.setUseAbleQuantity(request.getUseAbleQuantity());
        addData.setUsingQuantity(request.getUsingQuantity());
        addData.setRepairQuantity(request.getRepairQuantity());

        goodsRepository.save(addData);
    }
}
