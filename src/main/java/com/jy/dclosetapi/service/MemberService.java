package com.jy.dclosetapi.service;

import com.jy.dclosetapi.entity.Member;
import com.jy.dclosetapi.enums.MemberGrade;
import com.jy.dclosetapi.enums.MemberGroup;
import com.jy.dclosetapi.enums.PayDay;
import com.jy.dclosetapi.enums.PayWay;
import com.jy.dclosetapi.model.member.*;
import com.jy.dclosetapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getMember(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    /**
     * 신규 아이디인지 확인한다.
     *
     * @param username 아이디
     * @return true 신규아이디 / false 중복아이디
     */

    public MemberDupCheckResponse getMemberIdDubCheck(String username) {
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUsername(username));

        return response;
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    // 회원 가입 기능
    public void setMember(MemberJoinRequest request) throws Exception {
        if (!isNewUsername(request.getUsername())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();
        Member addData = new Member();
        addData.setUsername(request.getUsername());
        addData.setPassword(request.getPassword());
        addData.setPasswordRe(request.getPasswordRe());
        addData.setMemberName(request.getMemberName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setMemberAddress(request.getMemberAddress());
        addData.setMemberDetailedAddress(request.getMemberDetailedAddress());
        addData.setPostCode(request.getPostCode());
        addData.setSubscriptDate(LocalDateTime.now());
        addData.setNYPersonalInfo(request.getNYPersonalInfo());
        addData.setNYMarketing(request.getNYMarketing());
        // 등급과 권한을 회원가입시 기본값 일반회원으로 고정.
        addData.setMemberGrade(MemberGrade.NORMAL);
        addData.setMemberGroup(MemberGroup.NORMAL);
        addData.setPayWay(PayWay.NONE);
        addData.setPayInfo(null);
        addData.setPayDay(PayDay.NONE);

        memberRepository.save(addData);
    }

    // 회원 전체 조회
    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for(Member member : originList ) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setUsername(member.getUsername());
            addItem.setMemberName(member.getMemberName());
            addItem.setBirthDay(member.getBirthDay());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setMemberAddress(member.getMemberAddress());
            addItem.setMemberDetailedAddress(member.getMemberDetailedAddress());
            addItem.setPostCode(member.getPostCode());
            addItem.setSubscriptDate(member.getSubscriptDate());
            addItem.setNYPersonalInfo(member.getNYPersonalInfo());
            addItem.setNYMarketing(member.getNYMarketing());
            addItem.setMemberGrade(member.getMemberGrade().getMemberGrade());
            addItem.setPayWay(member.getPayWay().getPayWay());
            addItem.setPayInfo(member.getPayInfo());
            addItem.setPayDay(member.getPayDay().getPayDay());

            result.add(addItem);
        }

        return result;
    }

    // 단일 회원 id로 조회
    public MemberResponse getResponse(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setUsername(originData.getUsername());
        response.setMemberName(originData.getMemberName());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setMemberAddress(originData.getMemberAddress());
        response.setMemberDetailedAddress(originData.getMemberDetailedAddress());
        response.setPostCode(originData.getPostCode());
        response.setSubscriptDate(originData.getSubscriptDate());
        response.setNYPersonalInfo(originData.getNYPersonalInfo());
        response.setNYMarketing(originData.getNYMarketing());
        response.setMemberGrade(originData.getMemberGrade().getMemberGrade());
        response.setPayWay(originData.getPayWay().getPayWay());
        response.setPayInfo(originData.getPayInfo());
        response.setPayDay(originData.getPayDay().getPayDay());

        return response;
    }

    // 멤버십 가입 기능(회원 등급 변경, 결제 정보 추가)
    public void joinMembership(long id, Membership membership) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setMemberGrade(MemberGrade.MEMBERSHIP);
        originData.setPayWay(membership.getPayWay());
        originData.setPayInfo(membership.getPayInfo());
        originData.setPayDay(membership.getPayDay());

        memberRepository.save(originData);
    }

    // 회원 탈퇴 기능 ( 비회원으로 변경 )
    public void memberWithdrawal(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setMemberGrade(MemberGrade.NONE);

        memberRepository.save(originData);
    }
}
