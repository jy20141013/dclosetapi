package com.jy.dclosetapi.service;

import com.jy.dclosetapi.model.questionBulletin.*;
import com.jy.dclosetapi.repository.QuestionBulletInRepository;
import com.jy.dclosetapi.entity.Member;
import com.jy.dclosetapi.entity.QuestionBulletIn;
import com.jy.dclosetapi.repository.QuestionBulletInRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class QuestionBulletInService {



    // 게시판 C 등록
    private final QuestionBulletInRepository questionBulletInRepository;
    public void setQuestionBulletIn(Member member, QuestionBulletInCreateRequest request){
        QuestionBulletIn addData = new QuestionBulletIn();
        addData.setMember(member);
        addData.setQuestionCreateDate(request.getQuestionCreateDate());
        addData.setQuestionTitle(request.getQuestionTitle());
        addData.setQuestionContent(request.getQuestionContent());
        addData.setQuestionPassword(request.getQuestionPassword());

        questionBulletInRepository.save(addData);
    }

    // 게시판 전체 리스트 찾기 (복수 R)
    public List<QuestionBulletInItem> getQuestionBulletIns() {
        List<QuestionBulletIn> originList = questionBulletInRepository.findAll();
        List<QuestionBulletInItem> result = new LinkedList<>();

        for(QuestionBulletIn questionBulletIn : originList){
            QuestionBulletInItem addItem = new QuestionBulletInItem();
            addItem.setId(questionBulletIn.getId());
            addItem.setMemberId(questionBulletIn.getMember().getId());
            addItem.setQuestionTitle(questionBulletIn.getQuestionTitle());
            addItem.setQuestionContent(questionBulletIn.getQuestionContent());
            addItem.setQuestionPassword(questionBulletIn.getQuestionPassword());
            addItem.setQuestionCreateDate(questionBulletIn.getQuestionCreateDate());

            result.add(addItem);
        }
        return result;
    }

    // 단수 R id로 찾기
    public QuestionBulletInResponse getQuestionBulletIn (long id){

        QuestionBulletIn originData = questionBulletInRepository.findById(id).orElseThrow();

        QuestionBulletInResponse response = new QuestionBulletInResponse();

        response.setId(originData.getId());
        response.setQuestionTitle(originData.getQuestionTitle());
        response.setQuestionContent(originData.getQuestionContent());
        response.setQuestionPassword(originData.getQuestionPassword());
        response.setQuestionCreateDate(originData.getQuestionCreateDate());

        return response;
    }

    // 단수 R Member id로 찾기
    public QuestionBulletInMemberResponse getQuestionMemberBulletIn (long memberId){

        QuestionBulletIn originData = questionBulletInRepository.findById(memberId).orElseThrow();

        QuestionBulletInMemberResponse response = new QuestionBulletInMemberResponse();

        response.setMemberId(originData.getMember().getId());
        response.setQuestionTitle(originData.getQuestionTitle());
        response.setQuestionPassword(originData.getQuestionPassword());
        response.setQuestionContent(originData.getQuestionContent());
        response.setQuestionCreateDate(originData.getQuestionCreateDate());

        return response;
    }



    // id로 수정 하는 U
    public void putQuestionBulletInChangeRequest(long id, QuestionBulletInChangeRequest request){
        QuestionBulletIn originData = questionBulletInRepository.findById(id).orElseThrow();

        originData.setQuestionTitle(request.getQuestionTitle());
        originData.setQuestionContent(request.getQuestionContent());
        originData.setQuestionPassword(request.getQuestionPassword());

        questionBulletInRepository.save(originData);

    }


    // MemberId로  수정 하는 U
    public void putQuestionBulletInChangeMemberRequest (Member member, QuestionBulletInChangeMemberRequest request){
        QuestionBulletIn originData = questionBulletInRepository.findById(member.getId()).orElseThrow();

        originData.setQuestionTitle(request.getQuestionTitle());
        originData.setQuestionContent(request.getQuestionContent());
        originData.setQuestionPassword(request.getQuestionPassword());

        questionBulletInRepository.save(originData);
    }

    //삭제 기능 D
    public void delQuestionBulletIn(Member member) {questionBulletInRepository.deleteById(member.getId());}






}
