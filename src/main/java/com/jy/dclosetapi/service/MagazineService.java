package com.jy.dclosetapi.service;

import com.jy.dclosetapi.entity.Magazine;
import com.jy.dclosetapi.entity.Member;
import com.jy.dclosetapi.model.magazine.MagazineChangeRequest;
import com.jy.dclosetapi.model.magazine.MagazineCreateRequest;
import com.jy.dclosetapi.model.magazine.MagazineItem;
import com.jy.dclosetapi.model.magazine.MagazineResponse;
import com.jy.dclosetapi.repository.MagazineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MagazineService {
    private final MagazineRepository magazineRepository;

    public void setMagazine(Member member, MagazineCreateRequest request) {
        Magazine addData = new Magazine();
        addData.setMember(member);
        addData.setMagazineCreateDate(LocalDate.now());
        addData.setMagazineTitle(request.getMagazineTitle());
        addData.setMagazineImage(request.getMagazineImage());
        addData.setMagazineText(request.getMagazineText());
        addData.setCuratorRecommend1(request.getCuratorRecommend1());
        addData.setCuratorRecommend2(request.getCuratorRecommend2());
        addData.setCuratorRecommend3(request.getCuratorRecommend3());
        addData.setCuratorRecommend4(request.getCuratorRecommend4());

        magazineRepository.save(addData);
    }

    public List<MagazineItem> getMagazines() {
        List<Magazine> originList = magazineRepository.findAll();
        List<MagazineItem> result = new LinkedList<>();

        for(Magazine magazine : originList) {
            MagazineItem addItem = new MagazineItem();
            addItem.setId(magazine.getId());
            addItem.setMagazineTitle(magazine.getMagazineTitle());
            addItem.setMagazineImage(magazine.getMagazineImage());
            addItem.setMagazineText(magazine.getMagazineText());
            addItem.setCuratorRecommend1(magazine.getCuratorRecommend1());
            addItem.setCuratorRecommend2(magazine.getCuratorRecommend2());
            addItem.setCuratorRecommend3(magazine.getCuratorRecommend3());
            addItem.setCuratorRecommend4(magazine.getCuratorRecommend4());

            result.add(addItem);
        }

        return result;
    }

    public MagazineResponse getMagazine(long id) {
        Magazine originData = magazineRepository.findById(id).orElseThrow();
        MagazineResponse response = new MagazineResponse();
        response.setMagazineTitle(originData.getMagazineTitle());
        response.setMagazineImage(originData.getMagazineImage());
        response.setMagazineText(originData.getMagazineText());
        response.setCuratorRecommend1(originData.getCuratorRecommend1());
        response.setCuratorRecommend2(originData.getCuratorRecommend2());
        response.setCuratorRecommend3(originData.getCuratorRecommend3());
        response.setCuratorRecommend4(originData.getCuratorRecommend4());

        return response;
    }

    public void putMagazine(long id, MagazineChangeRequest request) {
        Magazine originData = magazineRepository.findById(id).orElseThrow();
        originData.setMagazineImage(request.getMagazineImage());
        originData.setMagazineTitle(request.getMagazineTitle());
        originData.setMagazineText(request.getMagazineText());
        originData.setCuratorRecommend1(request.getCuratorRecommend1());
        originData.setCuratorRecommend2(request.getCuratorRecommend2());
        originData.setCuratorRecommend3(request.getCuratorRecommend3());
        originData.setCuratorRecommend4(request.getCuratorRecommend4());

        magazineRepository.save(originData);
    }
}
