package com.jy.dclosetapi.service;

import com.jy.dclosetapi.entity.Member;
import com.jy.dclosetapi.entity.Orders;
import com.jy.dclosetapi.entity.Goods;
import com.jy.dclosetapi.enums.OrderStatus;
import com.jy.dclosetapi.model.order.OrderItem;
import com.jy.dclosetapi.model.order.OrderRequest;
import com.jy.dclosetapi.model.order.OrderResponse;
import com.jy.dclosetapi.repository.OrderRepository;
import io.swagger.v3.oas.models.links.Link;
import jakarta.persistence.criteria.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    public void setOrder(Member member, Goods goods, OrderRequest orderRequest) {
        Orders addData = new Orders();
        LocalDate deadLineDate = orderRequest.getDesiredDate().plusDays(10);
        addData.setMember(member);
        addData.setGoods(goods);
        addData.setOrderDate(LocalDate.now());
        addData.setDesiredDate(orderRequest.getDesiredDate());
        addData.setDeadlineDate(deadLineDate);
        addData.setOrderStatus(OrderStatus.RECEIVE);

        orderRepository.save(addData);
    }

    public List<OrderItem> getOrders() {
        List<Orders> originList = orderRepository.findAll();
        List<OrderItem> result = new LinkedList<>();

        for(Orders orders : originList) {
            OrderItem addItem = new OrderItem();
            addItem.setId(orders.getId());
            addItem.setOrderDate(orders.getOrderDate());
            addItem.setDesiredDate(orders.getDesiredDate());
            addItem.setDeadlineDate(orders.getDeadlineDate());
            addItem.setOrderStatus(orders.getOrderStatus());
            addItem.setDeliveryId(orders.getDeliveryId());
            addItem.setReturnId(orders.getReturnId());

            result.add(addItem);
        }

        return result;
    }

    public OrderResponse getOrder(long id, Member member, Goods goods) {
        Orders originData = orderRepository.findById(id).orElseThrow();
        OrderResponse response = new OrderResponse();
        response.setId(originData.getId());
        response.setMember(member);
        response.setGoods(goods);
        response.setOrderDate(originData.getOrderDate());
        response.setDesiredDate(originData.getDesiredDate());
        response.setDeadlineDate(originData.getDeadlineDate());
        response.setOrderStatus(originData.getOrderStatus());
        response.setDeliveryId(originData.getDeliveryId());
        response.setReturnId(originData.getReturnId());

        return response;
    }
}
