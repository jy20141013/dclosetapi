package com.jy.dclosetapi.model.order;

import com.jy.dclosetapi.entity.Goods;
import com.jy.dclosetapi.entity.Member;
import com.jy.dclosetapi.enums.MemberGrade;
import com.jy.dclosetapi.enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderItem {
    private Long id;
    private Member member;
    private Goods goods;
    private MemberGrade memberGrade;
    private LocalDate orderDate;
    private LocalDate desiredDate;
    private LocalDate deadlineDate;
    private OrderStatus orderStatus;
    private String deliveryId;
    private String returnId;
}
