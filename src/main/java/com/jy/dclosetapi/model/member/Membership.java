package com.jy.dclosetapi.model.member;

import com.jy.dclosetapi.enums.MemberGrade;
import com.jy.dclosetapi.enums.MemberGroup;
import com.jy.dclosetapi.enums.PayDay;
import com.jy.dclosetapi.enums.PayWay;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Membership {
    // 멤버십 가입 신청 시 멤버 등급, 결제수단에 대한 정보 변경

    private MemberGrade memberGrade;
    private PayWay payWay;
    private String payInfo;
    private PayDay payDay;
}
