package com.jy.dclosetapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    private String username;
    private String password;
    private String passwordRe;
    private String memberName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String memberAddress;
    private String memberDetailedAddress;
    private Integer postCode;
    private Boolean NYPersonalInfo;
    private Boolean NYMarketing;
}
