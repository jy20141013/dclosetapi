package com.jy.dclosetapi.model.magazine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MagazineItem {
    private Long id;
    private String magazineTitle;
    private String magazineImage;
    private String magazineText;
    private String curatorRecommend1;
    private String curatorRecommend2;
    private String curatorRecommend3;
    private String curatorRecommend4;
}
