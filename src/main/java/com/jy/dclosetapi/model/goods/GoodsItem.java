package com.jy.dclosetapi.model.goods;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GoodsItem {

    private LocalDate goodsCreateDate;
    private String goodsName;
    private Integer goodsCode;
    private String goodsInfo;
    private String goodsSize;
    private String goodsColor;
    private Boolean YnMembership;
    private Boolean YnOffline;
    private Boolean YnFree;
    private Boolean YnOneDay;
    private Boolean YnPost;
    private Integer useAbleQuantity;
    private Integer usingQuantity;
    private Integer repairQuantity;



}
