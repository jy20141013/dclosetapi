package com.jy.dclosetapi.model.goods;


import com.jy.dclosetapi.enums.GoodsSize;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class GoodsCreateRequest {

    private LocalDateTime productCreateDate;

    private String productName;

    private Integer productCode;

    private String productInfo;

    private GoodsSize goodsSize;

    private String productColor;

    private String productMainImage;

    private String productSubImage1;

    private String productSubImage2;

    private String productSubImage3;

    private Boolean YnMembership;

    private Boolean YnOffline;

    private Boolean YnFree;

    private Boolean YnOneDay;

    private Boolean YnPost;

    private Integer useAbleQuantity;

    private Integer usingQuantity;

    private Integer repairQuantity;





}
