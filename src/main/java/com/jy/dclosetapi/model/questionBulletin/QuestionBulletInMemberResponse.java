package com.jy.dclosetapi.model.questionBulletin;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class QuestionBulletInMemberResponse {


    private Long memberId;
    private LocalDate questionCreateDate;
    private String questionTitle;
    private Integer questionPassword;
    private String questionContent;

}
