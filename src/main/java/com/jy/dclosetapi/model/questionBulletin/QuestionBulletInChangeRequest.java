package com.jy.dclosetapi.model.questionBulletin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class QuestionBulletInChangeRequest {

    private String questionTitle;
    private Integer questionPassword;
    private String questionContent;

}
